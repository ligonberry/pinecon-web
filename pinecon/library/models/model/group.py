from enum import Enum


class GroupType(Enum):
    SYSTEM = "system"
    CUSTOM = "custom"


class Group:
    """ Model for a number of things considered as a collective unit. """

    def __init__(self, creator_id, title, **kwargs):
        self.creator_id = creator_id
        self.title = title

        prop_defaults = {
            "group_id": None,
            "group_type": GroupType.CUSTOM.value
        }
        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))


class GroupAttribute(Enum):
    CREATOR_ID = "creator_id"
    TITLE = "title"

    ID = "group_id"
    TYPE = "group_type"
