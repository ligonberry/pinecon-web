from datetime import datetime
from enum import Enum

from pinecon.library.models import helpers


class TaskStatus(Enum):
    DEFINED = "defined"
    CREATED = "created"
    WORK = "work"
    EXECUTED = "executed"
    OVERDUE = "overdue"


TASK_STATUS_TUPLE = ("created", "work", "executed")


class TaskType(Enum):
    SIMPLE = "simple"
    PLAN = "plan"


TASK_TYPE_TUPLE = ("simple", "plan")


class TaskPriority(Enum):
    LOW = "low"
    MID = "mid"
    HIGH = "high"
    CRITICAL = "critical"


TASK_PRIORITY_TUPLE = ("low", "mid", "high", "critical")


class Task:
    """ Model for a piece of work to be done or undertaken. """

    def __init__(self, creator_id, title, **kwargs):
        self.creator_id = creator_id
        self.title = title

        prop_defaults = {
            "task_id": None,
            "status": TaskStatus.CREATED.value,
            "task_type": TaskType.SIMPLE.value,
            "priority": TaskPriority.MID.value,
            "description": "",
            "subtask_dependency": helpers.NO,
            "creation_date": datetime.now().date(),
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

    def __hash__(self):
        return hash((self.creator_id, self.title, self.task_id, self.status, self.task_type, self.priority,
                     self.description, self.subtask_dependency, self.creation_date))

    def __eq__(self, other):
        return ((self.creator_id, self.title, self.task_id, self.status, self.task_type, self.priority,
                 self.description, self.subtask_dependency, self.creation_date) == (other.creator_id, other.title,
                                                                                    other.task_id, other.status,
                                                                                    other.task_type, other.priority,
                                                                                    other.description,
                                                                                    other.subtask_dependency,
                                                                                    other.creation_date))


class TaskAttribute(Enum):
    CREATOR_ID = "creator_id"
    TITLE = "title"

    ID = "task_id"
    STATUS = "status"
    TYPE = "task_type"
    PRIORITY = "priority"
    DESCRIPTION = "description"
    DEPENDENT = "subtask_dependency"
    CREATION_DATE = "date_create"
