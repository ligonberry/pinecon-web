from enum import Enum


class GroupToTaskRelation:
    """ Model for interaction between group and task. """
    def __init__(self, task_id, group_id, relation_id=None):
        self.group_id = group_id
        self.task_id = task_id

        self.relation_id = relation_id


class GroupToTaskRelationAttribute(Enum):
    TASK_ID = "task_id"
    GROUP_ID = "group_id"

    RELATION_ID = "relation_id"