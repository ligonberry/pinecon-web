from enum import Enum

from pinecon.library.models import helpers


class ExecutorToTaskRelation:
    """ Model for interaction between executor and task. """

    def __init__(self, executor_id, task_id, **kwargs):
        self.executor_id = executor_id
        self.task_id = task_id

        prop_defaults = {
            "relation_id": None,
            "relation_confirmed": helpers.NO
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))


class ExecutorToTaskRelationAttribute(Enum):
    EXECUTOR_ID = "executor_id"
    TASK_ID = "task_id"

    RELATION_ID = "relation_id"
    RELATION_CONFIRMED = "relation_confirmed"
