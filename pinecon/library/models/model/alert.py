from enum import Enum

from pinecon.library.models import helpers


class Alert:
    """ Model for situation in which people prepare themselves for something important
        that might happen soon

    """

    def __init__(self, task_id, start_date, date_interval, **kwargs):
        self.task_id = task_id
        self.start_date = start_date
        self.date_interval = date_interval

        prop_defaults = {
            "alert_id": None,
            "active": helpers.YES
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))


class AlertAttribute(Enum):
    TASK = "task_id"
    START_DATE = "start_date"
    DATE_INTERVAL = "date_interval"

    ID = "alert_id"
    ACTIVE = "active"
