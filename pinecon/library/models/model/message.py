from datetime import datetime
from enum import Enum


class Message:
    """ Model for a piece of information or a request that you send to someone or leave for them
        when you cannot speak to them directly.

    """
    def __init__(self, user_id, title, **kwargs):
        self.user_id = user_id
        self.title = title

        prop_defaults = {
            "message_id": None,
            "text": "",
            "date": datetime.now().date(),
            "time": datetime.now().time()
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))


class MessageAttribute(Enum):
    USER_ID = "user_id"
    TITLE = "title"

    ID = "task_id"
    TEXT = "text"
    DATE = "date"
    TIME = "time"