from pinecon.library.models.model.task import TaskAttribute, TASK_TYPE_TUPLE, TASK_PRIORITY_TUPLE, TASK_STATUS_TUPLE
from pinecon.library.models.model.group import GroupAttribute
from pinecon.library.models.model.plan import PlanAttribute
from pinecon.library.models.model.message import MessageAttribute
from pinecon.library.models.model.task_date import TaskDateAttribute
from pinecon.library.models.model.task_to_task import TaskToTaskRelationAttribute
from pinecon.library.models.model.group_to_task import GroupToTaskRelationAttribute
from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelationAttribute
from pinecon.library.models.model.alert import Alert
from pinecon.library.models.model.user import User


def get_representative_model(model, model_attributes_transformer):

    for key, value in vars(model).items():
        if key in model_attributes_transformer:
            setattr(model, key, model_attributes_transformer[key](value))
        elif value is None:
            setattr(model, key, "-")
    return model


def get_representative_models(models, model_attributes_transformer):

    return [get_representative_model(model, model_attributes_transformer) for model in models]




