""" This module contains log functions for this library.

    Classes:
    ----------------
        * LogLevel(Enum)
            - contains supported logging levels;
    Public Method:
    ----------------
        * log_function_call(function_to_decorate, level=LogLevel.DEBUG)
            - decorator to write logs of functions;
        * log_exception(function_to_decorate)
            - decorator to write logs of functions, which call exceptions;
        * get_pinecon_logger()
            - returns logger for pinecon library;
"""
import logging
import logging.config
import os
from enum import Enum
from functools import wraps


class LogLevel(Enum):
    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"
    CRITICAL = "CRITICAL"


def log_function_call(function_to_decorate, level=LogLevel.DEBUG):
    logger = get_pinecon_logger()
    message = "{function} called".format(function=function_to_decorate.__name__)
    levels = {
        LogLevel.DEBUG: logger.debug,
        LogLevel.INFO: logger.info,
        LogLevel.WARNING: logger.warning,
        LogLevel.ERROR: logger.error,
        LogLevel.CRITICAL: logger.critical
    }

    @wraps(function_to_decorate)
    def wrapper_around_function(*args, **kwargs):
        levels[level](message)
        return function_to_decorate(*args, **kwargs)

    return wrapper_around_function


def log_exception(function_to_decorate):
    logger = get_pinecon_logger()

    @wraps(function_to_decorate)
    def wrapper_around_function(*args, **kwargs):
        try:
            return function_to_decorate(*args, **kwargs)
        except Exception as e:
            logger.error(e, exc_info=1)
            raise
    return wrapper_around_function


def get_pinecon_logger():
    return logging.getLogger("pineconlogger.base")


def set_logger(log_level=LogLevel.DEBUG.value,
               log_format='[%(levelname)s] %(asctime)s - %(name)s: %(message)s',
               log_path=os.path.join(os.getcwd(), 'pinelog.log')):
    file_handler = logging.FileHandler(log_path)
    file_handler.setFormatter(logging.Formatter(log_format))

    logger = get_pinecon_logger()
    logger.setLevel(log_level)
    logger.addHandler(file_handler)


def reset_logger():
    logger = get_pinecon_logger()
    for handler in logger.handlers:
        logger.removeHandler(handler)
