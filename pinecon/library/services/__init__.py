"""
    This package provides modules to work with library at a higher level (with exception handling).

    Modules:
    ----------------
        exceptions - contains exceptions, that can rise within this package;
        exceptions_handlers - contains the basic functions for handling exceptions in this package;
        loggers - contains log functions for this library;
        service - pprovides a class for working with database data at a higher level (with error handling)

    How to use it:
    ----------------
        >>> import pinecon.library
        >>> from pinecon.library.services import service
        >>> from pinecon.library.storage import queries
        >>> from pinecon.library.storage import storage
        >>> connection_string = 'sqlite:///library.db'
        >>> data_storage = storage.Storage(connection_string)
        >>> queries = queries.Queries(data_storage)
        >>> serv = service.Service(queries)
        >>> from pinecon.library.models import model
        >>> user = serv.add_checked_user(model.User("Ligonberry"))
        >>> task = serv.add_checked_task(model.Task(user.user_id, "Learning Python"))
"""

