""" This module provides a class for working with database data at a higher level (with error handling)

    Classes:
    ----------------
        Service
            working with database data at a higher level (with logical error handling)
"""
from datetime import datetime
from typing import List, Tuple

from dateutil.relativedelta import relativedelta

from pinecon.library.models import helpers
from pinecon.library.models import representations
from pinecon.library.models.model.alert import Alert, AlertAttribute
from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelation
from pinecon.library.models.model.group import Group, GroupAttribute
from pinecon.library.models.model.group_to_task import GroupToTaskRelation
from pinecon.library.models.model.message import Message
from pinecon.library.models.model.plan import Plan, PlanEndReason, PlanAttribute, PlanRepetitionStyle
from pinecon.library.models.model.task import Task, TaskAttribute, TaskStatus, TaskType
from pinecon.library.models.model.task_date import TaskDate, TaskDateType, TaskDateAttribute
from pinecon.library.models.model.task_to_task import TaskToTaskRelation, TaskRelation, TaskToTaskRelationAttribute
from pinecon.library.models.model.user import User

from pinecon.library.services import exceptions, exception_validation
from pinecon.library.services.loggers import log_function_call, log_exception

from pinecon.library.storage import queries


class Service:
    """ Contains core functions to make checked queries to database.
        Extends query capabilities.

        Attributes:
        ----------------
        * querie (Queries)
            contains core functions to make queries to database;
        * duplication_ex_handler (DuplicationExceptionHandler)
            handles errors related to duplication;
        * logic_ex_handler (LogicalExceptionHandler)
            handles errors that violate application logic
        * rights_ex_handler (RightExceptionHandler)
            handles errors related to violation of rights

        Public Methods:
        ----------------
            1. Task services:
                * add_checked_task(self, task: model.Task)
                    - add task to the database;
                * get_checked_task_by_id(self, task_id: int)
                    - try get checked task by id from the database or raise exception, if operation is invalid;
                * get_all_checked_user_tasks(self)
                    - try get all user tasks from the database.;
                * view_task(self, task_id: int)
                    - return object of class-view for the defined task;
                * view_all_task_subtasks(self, task_id: int)
                    - return object of class-view, contains all task subtasks view;
                * try_execute_subtasks(self, task: model.Task)
                    - try execute task subtasks or raise exception, if operation is invalid;
                * try_execute_parent_tasks(self, task: model.Task)
                    - try execute subtask parent tasks (if subtask-dependency is 'yes') or raise exception,
                      if operation is invalid;
                * delete_checked_task(self, task_id: int)
                    - try delete checked task or raise exception, if operation is invalid;
                * update_checked_task(self, task_id: int, **kwargs)
                    - try update checked task or raise exception, if operation is invalid;
            2. Group services:
                * get_checked_group_by_id(self, group_id: int)
                    - try get checked group by id from the database or raise exception, if operation is invalid;
                * get_all_checked_group_tasks(self, group_id: int)
                    - try get all checked group tasks from the database or raise exception, if operation is invalid;
                * view_checked_group(self, group_id: int)
                    - return object of class-view for the defined group;
                * view_all_checked_group(self)
                    - return object of class-view, contains all user groups;
                * delete_checked_group_by_id(self, group_id: int)
                    - try delete checked group or raise exception, if operation is invalid;
                * update_checked_group(self, group_id: int, title: str)
                    - try update title of checked group or raise exception, if operation is invalid;
            3. Plan services:
                * add_checked_plan(self, template: model.Task, plan: model.Plan)
                    - try add checked plan to the database or raise exception, if operation is invalid;
                * get_checked_plan_by_id(self, plan_id: int)
                    - try get checked plan by id from the database or raise exception, if operation is invalid;
                * get_checked_plan(self, user_id)
                    -
                * update_checked_plan(self, plan_id: int, **kwargs)
                    - try to update plan;
                * view_checked_plan(self, plan_id: int)
                    - return object of class-view for the defined plan;
            4. Message services:
                * get_checked_message_by_id(self, message_id: int)
                    - try get checked message by id from the database or raise exception, if operation is invalid;
                * view_checked_messages(self)
                    - return object of class-view for the defined message;
            5. Date services:
                * add_checked_date(self, date: model.Date)
                    - try add checked date to the database or raise exception, if operation is invalid;
                * get_checked_date_by_id(self, date_id: int)
                    - try get checked date by id from the database or raise exception, if operation is invalid;
                * view_checked_date(self, date_id: int)
                    - return object of class-view for the defined date;
                * delete_checked_date_by_id(self, date_id: int)
                    - try delete checked date or raise exception, if operation is invalid;
                * update_checked_date(self, date_id: int, **kwargs)
                    - try update checked date or raise exception, if operation is invalid;
            6. TaskToTaskRelation services:
                * add_checked_task_to_task_relation(self, task_to_task_relation: model.TaskToTaskRelation)
                    - try add checked task-to-task relation to the database or raise exception, if operation is invalid;
                * get_checked_task_to_task_relation_by_id(self, relation_id: int)
                    - try get checked task-to-task relation by id from the database or raise exception,
                      if operation is invalid;
                * get_checked_task_to_task_relations(self, task_id)
                    - try get checked task-to-task relation by task id from the database or raise exception,
                      if operation is invalid;
                * get_checked_task_to_task_relations_by_child(self, task_id)
                    - try get checked task-to-task relation by chaild task id from the database or raise exception,
                      if operation is invalid;
                * get_checked_subtasks(self, task_id)
                    - try get checked task subtasks from the database or raise exception, if operation is invalid;
                * delete_checked_task_to_task_relation(self, parent_task_id: int, subtask_id: int)
                    - try delete checked task-to-task relation or raise exception, if operation is invalid;
                * update_checked_task_to_task_relation(self, relation_id: int, relation_type: str)
                    - try update type of checked task-to-task relation or raise exception, if operation is invalid;
            7. GroupToTaskRelation services:
                * add_checked_group_to_task_relation(self, relation: model.GroupToTaskRelation)
                    - try add checked group-to-task relation to the database or raise exception,
                      if operation is invalid;
                * delete_checked_group_to_task_relation(self, task_id: int, group_id: int)
                    - try delete checked group-to-task relation or raise exception, if operation is invalid;
            8. ExecutorToTaskRelation services:
                * add_checked_executor_to_task_relation(self, relation: ExecutorToTaskRelation)
                    - try add checked executor-to-task relation to the database or raise exception,
                      if operation is invalid;
                * delete_checked_executor_to_task_relation(self, task_id: int, executor_id: int)
                    - try delete checked executor-to-task relation or raise exception, if operation is invalid;
            9. Alert services:
                * add_checked_alert(self, alert: model.Alert)
                    - try add checked alert to the database or raise exception, if operation is invalid;
                * get_checked_alert_by_id(self, alert_id: int)
                    - try get checked alert by id from the database or raise exception, if operation is invalid;
                * view_checked_alert(self, alert_id: int)
                    - return object of class-view for the defined alert;
                * delete_checked_alert(self, alert_id: int)
                    - try delete checked alert or raise exception, if operation is invalid;
                * update_checked_alert(self, alert_id: int, **kwargs)
                    - try update checked alert or raise exception, if operation is invalid;
            10. User services:
                * add_checked_user(self, user: model.User)
                     - add user to the database;
                * get_checked_user(self)
                     - get user from database;
                * get_checked_user_by_username(self, username: str)
                    - get user from database by name;
                * get_all_checked_users(self)
                    - get all users from database;
                * get_checked_executors(self, task_id: int)
                    - try get all task executors from database or raise exception,
                      if operation is invalid;
                * view_all_user_tasks(self)
                     - return object of class-view with all tasks of the defined user;
                * is_creator(self, executor_id: int, task_id: int)
                     - check: user is creator?
            11. update_application(self)
                - update application: alerts, overdue plans and task;
    """

    def __init__(self, query, current_user_id=None):
        self.query = query
        self.current_user_id = current_user_id

        self.duplication_ex_handler = exception_validation.DuplicationExceptionHandler(query)
        self.logic_ex_handler = exception_validation.LogicalExceptionHandler(query)
        self.rights_ex_handler = exception_validation.RightExceptionHandler(query)

    #  region Task services: add, get, view, try to execute, delete and update checked tasks;
    @log_exception
    @log_function_call
    def add_checked_task(self, task: Task) -> Task:
        """ Add task to the database. """
        return self.query.add_task(task)

    @log_exception
    @log_function_call
    def get_checked_task_by_id(self, task_id: int) -> Task:
        """
        Try get checked task by id from the database or raise exception,
        if operation is invalid: user have no rights to work with this task.

        """
        self.rights_ex_handler.is_have_rights_task(self.current_user_id, task_id)
        return self.query.get_task_by_id(task_id)

    @log_exception
    @log_function_call
    def get_all_checked_user_tasks(self):
        """ Try get all user tasks from the database. """
        return self.query.get_all_user_tasks(self.current_user_id)

    # region view checked tasks

    @log_exception
    @log_function_call
    def view_task(self, task_id: int) -> representations.TaskRepresentation:
        """ Return object of class-view for the defined task. """

        task = self.get_checked_task_by_id(task_id)

        # Add information to view
        creator = self.query.get_user_by_id(task.creator_id)
        executors = self.query.get_executors(task_id)
        dates = self.query.get_all_task_dates(task_id)
        groups = self.query.get_all_groups_by_task_id(self.current_user_id, task_id)
        alerts = self.query.get_alerts_by_task_id(task_id)
        plan = self.query.get_task_plan(task_id)

        task_view = representations.TaskRepresentation(task, creator, executors, dates, groups,
                                                       alerts, plan)
        return task_view

    @log_exception
    @log_function_call
    def view_all_task_subtasks(self, task_id: int) -> representations.SubtasksRepresentation:
        """ Return object of class-view, contains all task subtasks views. """

        parent_task = self.get_checked_task_by_id(task_id)
        subtasks = self.query.get_all_task_subtasks(task_id)

        subtask_view = representations.SubtasksRepresentation(parent_task, "", "")

        def _get_subtasks_tree(parent_task_view, subtask) -> None:

            task_tree = self.query.get_all_task_subtasks(subtask.task_id)
            reltaion = self.query.get_task_to_task_relation(parent_task_view.task.task_id, subtask.task_id)

            child_view = representations.SubtasksRepresentation(subtask, reltaion.relation_type, reltaion.relation_id)

            parent_task_view.subtask_views_tree.append(child_view)

            for subtask_lockal in task_tree:
                _get_subtasks_tree(child_view, subtask_lockal)

        for subtask_ in subtasks:
            _get_subtasks_tree(subtask_view, subtask_)

        return subtask_view

    # endregion

    # region try to execute checked tasks
    @log_exception
    @log_function_call
    def try_execute_subtasks(self, task: Task):
        """
        Try execute task subtasks or raise exception,
        if operation is invalid: try execute task, that has subtask with bloking relations.

        """
        with queries.session_manager() as session:
            relations = self.query.get_all_task_to_task_relations_by_parent(task.task_id, session=session)
            for relation in relations:
                if relation.relation_type == TaskRelation.DEPENDS_ON.value:
                    self._handle_try_execute(relation.subtask)

    @log_exception
    @log_function_call
    def try_execute_parent_tasks(self, task: Task):
        """
        Try execute subtask parent tasks (if subtask-dependency is 'yes') or raise exception,
        if operation is invalid: try execute task, that has subtask with bloking relations.

        """
        with queries.session_manager() as session:
            parent_tasks = self.query.get_all_task_parent_tasks(task.task_id, session=session)

            for parent_task in parent_tasks:

                if parent_task.subtask_dependency == helpers.YES:
                    parent_subtasks = self.query.get_all_task_subtasks(parent_task.task_id, session=session)

                    # If all subtasks are executed => try execute this task
                    if all([parent_subtask.status == TaskStatus.EXECUTED.value for parent_subtask in
                            parent_subtasks]):
                        self._handle_try_execute(parent_task)

    @log_exception
    @log_function_call
    def _handle_try_execute(self, task: Task):
        if task.status == TaskStatus.EXECUTED.value:  # If task already execute.
            return

        self.logic_ex_handler.is_task_can_be_executed(task)

        args = {
            TaskAttribute.STATUS.value: TaskStatus.EXECUTED.value
        }

        task = self.query.update_task(task.task_id, args)

        self.try_execute_parent_tasks(task)

        self.try_execute_subtasks(task)

        self._alert_users_about_task_status_changed(task)

    # endregion

    @log_exception
    @log_function_call
    def delete_checked_task(self, task_id: int):
        """
        Try delete checked task or raise exception,
        if operation is invalid: user have no rights to work with this task.

        """

        self.rights_ex_handler.is_have_rights_task(self.current_user_id, task_id, executor=False)

        return self.query.delete_task_by_id(task_id)

    @log_exception
    @log_function_call
    def delete_checked_task_with_relations(self, task_id: int):
        groups = self.get_checked_filtered_groups(creator_id=self.current_user_id,
                                                  task_id=task_id)
        for group in groups:
            self.delete_checked_group_to_task_relation(task_id=task_id,
                                                       group_id=group.group_id)

        parent_relations = self.get_checked_task_to_task_relations(task_id)
        child_relations = self.get_checked_task_to_task_relations_by_child(task_id)
        for _, task in parent_relations:
            self.delete_checked_task_to_task_relation(parent_task_id=task_id,
                                                      subtask_id=task.task_id)
        for _, task in child_relations:
            self.delete_checked_task_to_task_relation(parent_task_id=task.task_id,
                                                      subtask_id=task_id)

        executors = self.get_checked_executors(task_id)
        for executor in executors:
            self.delete_checked_executor_to_task_relation(task_id, executor.user_id)

        dates = self.get_all_checked_task_dates(task_id)
        for date in dates:
            self.delete_checked_date_by_id(date.date_id)

        plans = self.get_checked_filtered_plans(user_id=self.current_user_id,
                                                task_id=task_id)
        for plan in plans:
            self.delete_checked_plan(plan.plan_id)

        return self.delete_checked_task(task_id)

    # region update checked tasks
    @log_exception
    @log_function_call
    def update_checked_task(self,
                            task_id: int,
                            title=None,
                            status=None,
                            task_type=None,
                            priority=None,
                            description=None,
                            subtask_dependency=None
                            ) -> Task:
        """
        Try update checked task or raise exception,
        if operation is invalid: user have no rights to work with this task.

        """

        self.rights_ex_handler.is_have_rights_task(self.current_user_id, task_id)

        args = {}

        if title:
            args[TaskAttribute.TITLE.value] = title

        # defined - special status for template task.
        if status not in ['defined', 'created']:
            self._update_checked_task_status(task_id, status=status)
            args[TaskAttribute.STATUS.value] = status

        if task_type:
            args[TaskAttribute.TYPE.value] = task_type

        if priority:
            args[TaskAttribute.PRIORITY.value] = priority

        if description:
            args[TaskAttribute.DESCRIPTION.value] = description

        if subtask_dependency:
            args[TaskAttribute.DEPENDENT.value] = subtask_dependency

        return self.query.update_task(task_id, args)

    @log_exception
    @log_function_call
    def _update_checked_task_status(self, task_id: int, status: str):

        self.logic_ex_handler.is_plan(task_id)

        task = self.get_checked_task_by_id(task_id)

        if status == TaskStatus.EXECUTED.value:
            self._handle_try_execute(task)
        else:
            args = {
                TaskAttribute.STATUS.value: status
            }
            task = self.query.update_task(task_id, args)

            self._alert_users_about_task_status_changed(task)

        return task

    @log_exception
    @log_function_call
    def _update_all_overdue_tasks(self):
        with queries.session_manager() as session:

            overdue_dates = []

            for date in self.query.get_all_overdue_dates(session=session):
                if date.date_type != TaskDateType.START.value:
                    overdue_dates.append(date)

            for overdue_date in overdue_dates:
                overdue_task = overdue_date.task
                self._change_status_to_overdue(overdue_task, overdue_date, session=session)

    @log_exception
    @log_function_call
    def _change_status_to_overdue(self, overdue_task: Task, overdue_date: TaskDate, session=None):

        if (overdue_task.status == TaskStatus.WORK.value or
                overdue_task.status == TaskStatus.CREATED.value or
                overdue_task.status == TaskStatus.DEFINED.value):
            overdue_task.status = TaskStatus.OVERDUE.value

            self._alert_about_change_status_to_overdue(overdue_task, overdue_date, session=session)

    @log_exception
    @log_function_call
    def _alert_about_change_status_to_overdue(self, overdue_task: Task, overdue_date: TaskDate, session=None):

        message_title = 'Task "{title}" with id {id_} was overdue!'.format(title=overdue_task.title,
                                                                           id_=overdue_task.task_id)

        message_text = 'Task will move to overdue tasks after {date}'.format(date=overdue_date.date)

        self._alert_users_about_task_changes(overdue_task, message_title, message_text, session=session)

    # endregion

    # endregion

    # region Group: add, get, view, delete and update checked groups;
    @log_exception
    @log_function_call
    def add_checked_group(self, group: Group):
        """ Add group to the database. """
        return self.query.add_group(group)

    @log_exception
    @log_function_call
    def get_checked_group_by_id(self, group_id: int) -> Group:
        """
        Try get checked group by id from the database or raise exception,
        if operation is invalid: user have no rights to work with this group.

        """
        self.rights_ex_handler.is_have_rights_group(self.current_user_id, group_id)
        return self.query.get_group_by_id(group_id)

    @log_exception
    @log_function_call
    def get_all_checked_group_tasks(self, group_id: int) -> List[Task]:
        """
        Try get all checked group tasks from the database or raise exception,
        if operation is invalid: user have no rights to work with this group.

        """
        self.rights_ex_handler.is_have_rights_group(self.current_user_id, group_id)
        return self.query.get_all_group_tasks(group_id)

    @log_exception
    @log_function_call
    def view_checked_group(self, group_id: int) -> representations.GroupRepresentation:
        """ Return object of class-view for the defined group. """
        group = self.get_checked_group_by_id(group_id)
        group_tasks = self.get_all_checked_group_tasks(group_id)
        return representations.GroupRepresentation(group, group_tasks)

    @log_exception
    @log_function_call
    def view_all_checked_group(self) -> representations.GroupsRepresentation:
        """ Return object of class-view, contains all user groups. """
        user_groups = self.query.get_groups_by_user_id(self.current_user_id)
        return representations.GroupsRepresentation(user_groups)

    @log_exception
    @log_function_call
    def delete_checked_group_by_id(self, group_id: int):
        """
        Try delete checked group or raise exception,
        if operation is invalid: user have no rights to work with this group.

        """
        self.rights_ex_handler.is_have_rights_group(self.current_user_id, group_id)
        return self.query.delete_group_by_id(group_id)

    @log_exception
    @log_function_call
    def delete_checked_group_with_relations(self, group_id: int):
        tasks = self.get_all_checked_group_tasks(group_id)
        for task in tasks:
            self.delete_checked_group_to_task_relation(task_id=task.task_id,
                                                       group_id=group_id)
        return self.delete_checked_group_by_id(group_id)

    @log_exception
    @log_function_call
    def update_checked_group(self, group_id: int, title: str) -> Group:
        """

        Try update title of checked group or raise exception,
        if operation is invalid: user have no rights to work with this group.
        """
        self.rights_ex_handler.is_have_rights_group(self.current_user_id, group_id)
        args = {
            GroupAttribute.TITLE.value: title
        }
        return self.query.update_group(group_id, args)

    # endregion

    # region Plan: add, get and view checked plans;
    @log_exception
    @log_function_call
    def add_checked_plan(self, template: Task, plan: Plan) -> Tuple[Task, Plan]:
        """
        Try add checked plan by id from the database or raise exception,
        if operation is invalid: user try to add plan, which end-reason is 'number',
        but don't enter number of repetitions, until plan is finished.

        """

        template = self.query.add_task(template)

        plan.task_id = template.task_id

        try:
            self.logic_ex_handler.is_can_add_plan(plan)
            plan = self.query.add_plan(plan)
        except Exception as e:
            self.query.delete_task_by_id(template.task_id)
            raise e

        return template, plan

    @log_exception
    @log_function_call
    def get_checked_plan_by_id(self, plan_id: int) -> Plan:
        """
        Try get checked plan by id from the database or raise exception,
        if operation is invalid: user have no rights to work with this plan.

        """
        self.rights_ex_handler.is_have_rights_plan(self.current_user_id, plan_id)
        return self.query.get_plan_by_id(plan_id)

    @log_exception
    @log_function_call
    def get_checked_plan(self, user_id):
        """ Try get plan by user_id from the database. """
        return self.query.get_all_user_plans(user_id)

    @log_exception
    @log_function_call
    def view_checked_plan(self, plan_id: int) -> representations.PlanRepresentation:
        """ Rreturn object of class-view for the defined plan. """
        plan = self.get_checked_plan_by_id(plan_id)
        plan_task = self.query.get_task_by_id(plan.task_id)
        return representations.PlanRepresentation(plan, plan_task)

    @log_exception
    @log_function_call
    def delete_checked_plan(self, plan_id: int):
        """
        Try delete checked plan

        """

        return self.query.delete_plan_by_id(plan_id)

    @log_exception
    @log_function_call
    def update_checked_plan(self,
                            plan_id: int,
                            start_date=None,
                            repeat_style=None,
                            interval_counter=None,
                            end_reason=None,

                            end_date=None,
                            count=None,
                            active=None) -> Plan:
        """ Try to update plan. """
        args = {}
        self.rights_ex_handler.is_have_rights_plan(self.current_user_id, plan_id)

        if start_date:
            args_[PlanAttribute.START_DATE.value] = start_date
        if repeat_style:
            args_[PlanAttribute.REPEAT_STYLE.value] = repeat_style
        if interval_counter:
            args_[PlanAttribute.INTERVAL_COUNTER.value] = interval_counter
        if end_reason:
            args_[PlanAttribute.END_REASON.value] = end_reason
        if end_date:
            args_[PlanAttribute.END_DATE.value] = end_date
        if count:
            args_[PlanAttribute.COUNT.value] = count
        if active:
            args_[PlanAttribute.ACTIVE.value] = active

        return self.query.update_plan(plan_id, args)

    @log_exception
    @log_function_call
    def _update_finished_plans(self):
        """ Update all finished plans. """
        with queries.session_manager() as session:

            finished_plans = self.query.get_all_finished_plans(session=session)

            for finished_plan in finished_plans:

                if (finished_plan.end_reason == PlanEndReason.AFTER_DATE.value and
                        datetime.now().date() > finished_plan.end_date):
                    finished_plan.active = helpers.NO
                    continue

                self._make_template_copy(finished_plan, session)

                self._is_plan_overdue(finished_plan)

                self._create_new_template_lifetime_(finished_plan)

                self._try_deactivate_plan(finished_plan)

    @log_exception
    @log_function_call
    def _make_template_copy(self, finished_plan: Plan, session):

        template = self.query.get_task_by_id(finished_plan.task_id, session=session)

        template_copy = Task(
            creator_id=template.creator_id,
            title=template.title,

            status=TaskStatus.CREATED.value,
            task_type=TaskType.SIMPLE.value,
            priority=template.priority,
            description=template.description,
            subtask_dependency=template.subtask_dependency,
            creation_date=datetime.now().date()
        )
        new_task = self.query.add_task(template_copy)
        self._alert_user_create_new_template_copy(new_task, finished_plan, session)

    @log_exception
    @log_function_call
    def _alert_user_create_new_template_copy(self, new_task: Task, finished_plan: Plan, session):
        message_title = 'Message title'
        message_text = "Template task of plan {id} was created!".format(id=finished_plan.plan_id)
        self._alert_users_about_task_changes(new_task, message_title, message_text, session)

    @staticmethod
    @log_exception
    @log_function_call
    def _is_plan_overdue(finished_plan: Plan):
        finished_plan.counter += 1

        if (finished_plan.end_reason == PlanEndReason.AFTER_NUMBER_OF_TIMES.value and
                finished_plan.counter == finished_plan.count):
            finished_plan.active = helpers.NO

    @staticmethod
    @log_exception
    @log_function_call
    def _create_new_template_lifetime_(finished_plan: Plan):
        time_interval = {
            PlanRepetitionStyle.DAILY.value: relativedelta(days=finished_plan.interval_counter),
            PlanRepetitionStyle.WEEKLY.value: relativedelta(weeks=finished_plan.interval_counter),
            PlanRepetitionStyle.MONTHLY.value: relativedelta(month=finished_plan.interval_counter),
            PlanRepetitionStyle.YEARLY.value: relativedelta(years=finished_plan.interval_counter)
        }

        finished_plan.start_date = finished_plan.start_date + time_interval[finished_plan.repeat_style]

    @staticmethod
    @log_exception
    @log_function_call
    def _try_deactivate_plan(finished_plan: Plan):
        if (finished_plan.end_reason == PlanEndReason.AFTER_DATE and
                finished_plan.start_date > finished_plan.end_date):
            finished_plan.active = helpers.NO

    # endregion

    # region Message: get, view and delete checked messages;
    @log_exception
    @log_function_call
    def get_checked_message_by_id(self, message_id: int) -> Message:
        """
        Try get checked message by id from the database or raise exception,
        if operation is invalid: user have no rights, to work with this message.

        """
        self.rights_ex_handler.is_have_rights_message(self.current_user_id, message_id)
        return self.query.get_message_by_id(message_id)

    @log_exception
    @log_function_call
    def view_checked_messages(self) -> representations.MessagesRepresentation:
        """ Rreturn object of class-view for the defined message. """
        messages = self.query.get_messages_by_user_id(self.current_user_id)
        return representations.MessagesRepresentation(messages)

    @log_exception
    @log_function_call
    def delete_checked_message(self, message_id: int):
        self.rights_ex_handler.is_have_rights_message(self.current_user_id, message_id)
        return self.query.delete_message_by_id(message_id)

    # endregion

    # region Date: add, get, view, delete and update checked dates;
    @log_exception
    @log_function_call
    def add_checked_date(self, date: TaskDate) -> TaskDate:
        """
        Try add checked date to the database or raise exception,
        if operation is invalid:

            1. user have no rightsto work with task, to wich he
               wants add date;
            2. user try to add plan, which end-reason is 'number',
               but don't enter number of repetitions, until plan is finished.
            3. checking the logic of the added date (more information in exception_handlers)

        """
        self.rights_ex_handler.is_have_rights_task(self.current_user_id, date.task_id)

        self.logic_ex_handler.is_plan(date.task_id)
        self.logic_ex_handler.is_can_add_date(date)

        return self.query.add_date(date)

    @log_exception
    @log_function_call
    def get_checked_date_by_id(self, date_id: int) -> TaskDate:
        """
        Try get checked date by id from the database or raise exception,
        if operation is invalid: user have no rights to work with this date.

        """
        self.rights_ex_handler.is_have_rights_date(self.current_user_id, date_id)
        return self.query.get_date_by_id(date_id)

    @log_exception
    @log_function_call
    def get_all_checked_task_dates(self, task_id: int) -> List[TaskDate]:
        self.rights_ex_handler.is_have_rights_task(self.current_user_id, task_id)
        return self.query.get_all_task_dates(task_id)

    @log_exception
    @log_function_call
    def view_checked_date(self, date_id: int) -> representations.TaskDateRepresentation:
        """ Return object of class-view for the defined date. """
        date = self.get_checked_date_by_id(date_id)
        task = self.query.get_date_task(date_id)
        return representations.TaskDateRepresentation(date, task)

    @log_exception
    @log_function_call
    def delete_checked_date_by_id(self, date_id: int):
        """
        Try delete checked date or raise exception,
        if operation is invalid: user have no rights to work with this date.

        """
        self.rights_ex_handler.is_have_rights_date(self.current_user_id, date_id)
        return self.query.delete_date_by_id(date_id)

    @log_exception
    @log_function_call
    def update_checked_date(self, date_id: int, **kwargs) -> TaskDate:
        """
        Try update checked date or raise exception,
        if operation is invalid:
            1. user have no rights to work with this date;
            2. cannot update data to a date in the past;

        """

        self.rights_ex_handler.is_have_rights_date(self.current_user_id, date_id)

        prop_defaults = {
            "date_type": None,
            "date": None  # Not model.Date object. Just simple date (datetime)!
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        args = {}

        if self.date_type is not None:
            args[TaskDateAttribute.TYPE.value] = self.date_type

        if self.date is not None:
            args[TaskDateAttribute.DATE.value] = self.date

        if TaskDateAttribute.DATE.value in args:
            self.logic_ex_handler.is_can_update_date(date_id, args[TaskDateAttribute.DATE.value])
        return self.query.update_date(date_id, args)

    # endregion

    # region TaskToTaskRelation: add, delete and update checked task-to-task relations;
    @log_exception
    @log_function_call
    def add_checked_task_to_task_relation(self,
                                          task_to_task_relation: TaskToTaskRelation) -> TaskToTaskRelation:
        """
        Try add checked task-to-task relation to the database or raise exception,
        if operation is invalid:
            1. user have no rights to task-to-task relation;
            2. user try to add plan, which end-reason is 'number',
               but don't enter number of repetitions, until plan is finished.
            3. user try to add one more duplicate relation;
            4. user try to add loop relation (task1 -> task2 -> task3, but task3 -> task1)

        """

        self.rights_ex_handler.is_have_rights_task_to_task_relation(self.current_user_id,
                                                                    task_to_task_relation.parent_task_id,
                                                                    task_to_task_relation.subtask_id,
                                                                    executor=False)

        self.logic_ex_handler.is_plan(task_to_task_relation.subtask_id)
        self.logic_ex_handler.is_plan(task_to_task_relation.parent_task_id)

        self.duplication_ex_handler.is_task_to_task_relation_duplication(task_to_task_relation)
        self.logic_ex_handler.is_task_to_task_relation_has_loop(task_to_task_relation.parent_task_id,
                                                                task_to_task_relation.subtask_id)

        return self.query.add_task_to_task_relation(task_to_task_relation)

    @log_exception
    @log_function_call
    def get_checked_task_to_task_relation_by_id(self, relation_id: int) -> TaskToTaskRelation:
        """
        Try get checked task-to-task relation by id from the database or raise exception,
        if operation is invalid: user have no rights to task-to-task relation.
        """
        self.rights_ex_handler.is_have_rights_task_to_task_relation_by_id(self.current_user_id,
                                                                          relation_id=relation_id)
        return self.query.get_task_to_task_relation_by_id(relation_id)

    @log_exception
    @log_function_call
    def get_checked_task_to_task_relations(self, task_id: int) -> Tuple[TaskToTaskRelation, Task]:
        """
        Try get checked task-to-task relation by task id from the database or raise exception,
        if operation is invalid: user have no rights to task-to-task relation.
        """
        self.rights_ex_handler.is_have_rights_task(self.current_user_id, task_id=task_id)
        relations = self.query.get_all_task_to_task_relations_by_parent(task_id)
        tasks = [self.query.get_task_by_id(relation.subtask_id) for relation in relations]
        return zip(relations, tasks)

    @log_exception
    @log_function_call
    def get_checked_task_to_task_relations_by_child(self, task_id: int) -> Tuple[TaskToTaskRelation, Task]:
        """
        Try get checked task-to-task relation by child task id from the database or raise exception,
        if operation is invalid: user have no rights to task-to-task relation.
        """
        self.rights_ex_handler.is_have_rights_task(self.current_user_id, task_id=task_id)
        relations = self.query.get_all_task_to_task_relations_by_child(task_id)
        tasks = [self.query.get_task_by_id(relation.parent_task_id) for relation in relations]
        return zip(relations, tasks)

    @log_exception
    @log_function_call
    def get_checked_subtasks(self, task_id: int) -> List[Task]:
        """
        Try get checked task subtasks from the database or raise exception,
        if operation is invalid: user have no rights to task-to-task relation.
        """
        self.rights_ex_handler.is_have_rights_task(self.current_user_id, task_id=task_id)
        return self.query.get_all_task_subtasks(task_id)

    @log_exception
    @log_function_call
    def delete_checked_task_to_task_relation(self, parent_task_id: int, subtask_id: int):
        """
        Try delete checked task-to-task relation or raise exception,
        if operation is invalid: user have no rights to work with this task-to-task relation.

        """
        self.rights_ex_handler.is_have_rights_task_to_task_relation(self.current_user_id, parent_task_id, subtask_id)
        return self.query.delete_task_to_task_relation(parent_task_id, subtask_id)

    @log_exception
    @log_function_call
    def update_checked_task_to_task_relation(self, relation_id: int, relation_type: str) -> TaskToTaskRelation:
        """
        Try update type of checked task-to-task relation or raise exception,
        if operation is invalid: user have no rights to work with this task-to-task relation.

        """

        relation = self.query.get_task_to_task_relation_by_id(relation_id)

        self.rights_ex_handler.is_have_rights_task_to_task_relation(self.current_user_id, relation.parent_task_id,
                                                                    relation.subtask_id)

        args = {
            TaskToTaskRelationAttribute.RELATION_TYPE.value: relation_type
        }

        return self.query.update_task_to_task_relation(relation_id, args)

    # endregion

    # region GroupToTaskRelation: add and delete checked group-to-task relations;
    @log_exception
    @log_function_call
    def add_checked_group_to_task_relation(self, relation: GroupToTaskRelation) -> GroupToTaskRelation:
        """
        Try add checked group-to-task relation to the database or raise exception,
        if operation is invalid:
            1. user have no rights to work with this group-to-task relation;
            2. user try to add one more duplicate relation.

        """
        self.rights_ex_handler.is_have_rights_group_to_task_relation(self.current_user_id, relation.group_id,
                                                                     relation.task_id)
        self.duplication_ex_handler.is_group_to_task_relation_duplication(relation)
        return self.query.add_group_to_task_relation(relation)

    @log_exception
    @log_function_call
    def delete_checked_group_to_task_relation(self, task_id: int, group_id: int):
        """
        Try delete checked group-to-task relation or raise exception,
        if operation is invalid: user have no rights to work with this group.

        """
        self.rights_ex_handler.is_have_rights_group(self.current_user_id, group_id)
        return self.query.delete_group_to_task_relation(task_id, group_id)

    # endregion

    # region ExecutorToTaskRelation: add and delete checked executor-to-task relations;
    @log_exception
    @log_function_call
    def add_checked_executor_to_task_relation(self,
                                              relation: ExecutorToTaskRelation) -> ExecutorToTaskRelation:
        """
        Try add checked executor-to-task relation to the database or raise exception,
        if operation is invalid:
            1. user have no rights to work with this task;
            2. user try to add one more duplicate relation.
            3. user try to add plan, which end-reason is 'number',
               but don't enter number of repetitions, until plan is finished.

        """
        self.rights_ex_handler.is_have_rights_task(self.current_user_id, relation.task_id, executor=False)
        self.duplication_ex_handler.is_executor_to_task_duplication(relation)
        self.logic_ex_handler.is_plan(relation.task_id)
        return self.query.add_executor_to_task_relation(relation)

    @log_exception
    @log_function_call
    def delete_checked_executor_to_task_relation(self, task_id: int, executor_id: int):
        """
        Try delete checked executor-to-task relation or raise exception,
        if operation is invalid:
            1. user have no rights to work with this task.

        """
        task = self.get_checked_task_by_id(task_id)

        if task.creator_id != self.current_user_id:
            self.query.delete_task_from_user_groups(self.current_user_id, task_id)

        return self.query.delete_executor_to_task_relation(executor_id, task_id)

    # endregion

    # region Alert: add, get, view, delete and update checked alerts;
    @log_exception
    @log_function_call
    def add_checked_alert(self, alert: Alert) -> Alert:
        """
        Try add checked alert to the database or raise exception,
        if operation is invalid:
            1. user have no rights to work with this task;
            2. user try to add plan, which end-reason is 'number',
               but don't enter number of repetitions, until plan is finished.

        """
        self.rights_ex_handler.is_have_rights_task(self.current_user_id, alert.task_id)
        self.logic_ex_handler.is_plan(alert.task_id)
        return self.query.add_alert(alert)

    @log_exception
    @log_function_call
    def get_checked_alert_by_id(self, alert_id: int) -> Alert:
        """
        Try get checked alert by id from the database or raise exception,
        if operation is invalid:
            1. user have no rights to work with this alert;

        """
        self.rights_ex_handler.is_have_rights_alert(self.current_user_id, alert_id)
        return self.query.get_alert_by_id(alert_id)

    @log_exception
    @log_function_call
    def view_checked_alert(self, alert_id: int) -> representations.AlertRepresentation:
        """ Return object of class-view for the defined alert. """
        alert = self.get_checked_alert_by_id(alert_id)
        task = self.get_checked_task_by_id(alert.task_id)
        return representations.AlertRepresentation(alert, task)

    @log_exception
    @log_function_call
    def delete_checked_alert(self, alert_id: int):
        """
        Try delete checked alert or raise exception,
        if operation is invalid:
            1. user have no rights to work with this alert;

        """
        self.rights_ex_handler.is_have_rights_alert(self.current_user_id, alert_id)
        return self.query.delete_alert_by_id(alert_id)

    @log_exception
    @log_function_call
    def update_checked_alert(self, alert_id: int, **kwargs) -> Alert:
        """
        Try update checked alert or raise exception,
        if operation is invalid:
            1. user have no rights to work with this alert;

        """
        prop_defaults = {
            "start_date": None,
            "date_interval": None,
            "active": None,
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        self.rights_ex_handler.is_have_rights_alert(self.current_user_id, alert_id)

        args = {}

        def choose_strategy():
            start_date_strategy(args),
            date_interval_strategy(args),
            active_strategy(args),

        def start_date_strategy(args_):
            if self.start_date is not None:
                args_[AlertAttribute.START_DATE.value] = self.start_date

        def date_interval_strategy(args_):
            if self.date_interval is not None:
                args_[AlertAttribute.DATE_INTERVAL.value] = self.date_interval

        def active_strategy(args_):
            if self.active is not None:
                args_[AlertAttribute.ACTIVE.value] = self.active

        choose_strategy()

        return self.query.update_alert(alert_id, args)

    # region alert users
    @log_exception
    @log_function_call
    def _alert_users_about_task_changes(self, task: Task, message_title: str, message_text: str, session=None):

        creator = self.query.get_user_by_id(task.creator_id, session=session)
        executors = self.query.get_executors(task.task_id, session=session)
        executors.append(creator)
        for user in executors:
            greeting_text = "Hi {name} ;) \n {title} \n {text}".format(name=user.name, title=message_title,
                                                                       text=message_text)

            message = Message(user.user_id, message_title, text=greeting_text)
            self.query.add_message(message, session=session)

    @log_exception
    @log_function_call
    def _alert_users_about_task_status_changed(self, task: Task):

        creator = self.query.get_user_by_id(task.creator_id)
        executors = self.query.get_executors(task.task_id)
        executors.append(creator)

        for user in executors:
            message_text = ('Hi {name} ;) There is some changes with status of your task "{title}" : '
                            'new status is "{status}" ').format(name=user.name, title=task.title, status=task.status)
            message_title = 'Task "{title}" was changed her status. New status - {status}'.format(title=task.title,
                                                                                                  status=task.status)

            message = Message(user.user_id, message_title, text=message_text)

            self.query.add_message(message)

    # endregion
    @log_exception
    @log_function_call
    def _update_alerts(self):
        """ Update all alerts as needed. """
        with queries.session_manager() as session:
            overdue_alerts = self.query.get_all_overdue_alerts(session=session)

            for overdue_alert in overdue_alerts:
                task = overdue_alert.task
                creator = task.creator

                executors_to_remind = self.query.get_executors_to_alert(overdue_alert.alert_id, session=session)
                executors_to_remind.append(creator)

                self._generate_alert(executors_to_remind, task, session)

                self._update_next_alert_date(overdue_alert)

    @log_exception
    @log_function_call
    def _generate_alert(self, executors_to_remind: List[User],
                        task: Task, session):
        for user in executors_to_remind:
            message_text = "Hi, {name}, don't forget about '{title}' task (id {task_id})!".format(name=user.name,
                                                                                                  title=task.title,
                                                                                                  task_id=task.task_id)
            message_title = 'Alert of "{title}" task.'.format(title=task.title)

            message = Message(user_id=user.user_id, text=message_text, title=message_title)

            self.query.add_message(message, session=session)

    @staticmethod
    @log_exception
    @log_function_call
    def _update_next_alert_date(overdue_alert: Alert):
        overdue_alert.start_date = overdue_alert.start_date + relativedelta(
            days=overdue_alert.date_interval)

    # endregion

    # region User: view checked users;
    @log_exception
    @log_function_call
    def add_checked_user(self, user: User) -> User:
        """ Add user to the database. """
        return self.query.add_user(user)

    @log_exception
    @log_function_call
    def get_checked_user(self) -> User:
        """ Get user from database. """
        return self.query.get_user_by_id(self.current_user_id)

    @log_exception
    @log_function_call
    def get_checked_user_by_username(self, username: str) -> User:
        """ Get user by username from database. """
        return self.query.get_user_by_username(username)

    @log_exception
    @log_function_call
    def get_all_checked_users(self) -> List[User]:
        """ Get all users from database. """
        return self.query.get_users()

    @log_exception
    @log_function_call
    def get_checked_executors(self, task_id: int):
        """
        Try get all task executors from database or raise exception,
        if operation is invalid: user have no rights to work with this task.
        """
        self.rights_ex_handler.is_have_rights_task(user_id=self.current_user_id, task_id=task_id)
        return self.query.get_executors(task_id)

    @log_exception
    @log_function_call
    def view_all_user_tasks(self) -> representations.TasksRepresentation:
        """ Return object of class-view with all tasks of the defined user. """
        creator = self.query.get_all_user_tasks(self.current_user_id)
        executor = self.query.get_all_executor_tasks(self.current_user_id)

        return representations.TasksRepresentation(creator, executor)

    @log_exception
    @log_function_call
    def is_creator(self, executor_id: int, task_id: int):
        """ Check: user is creator? """
        task = self.get_checked_task_by_id(task_id)
        if task.creator_id == executor_id:
            return True
        return False

    # endregion

    # region Filters: task, group and plan filters;
    @log_exception
    @log_function_call
    def get_checked_filtered_tasks(self,
                                   task_id: int=None,
                                   creator_id: int=None,
                                   status: str=None,
                                   type: str=None,
                                   executor_id: int=None,
                                   title_pattern: str=None,
                                   ):

        filter_ = {}
        if task_id:
            filter_[TaskAttribute.ID.value] = task_id
        if creator_id:
            creator = self.get_checked_user()
            if creator is None:
                raise exceptions.NotFoundError(creator_id, "creator")
            creator_id = creator.user_id
            filter_[TaskAttribute.CREATOR_ID.value] = creator_id
        if status:
            filter_[TaskAttribute.STATUS.value] = status
        if type:
            filter_[TaskAttribute.TYPE.value] = type

        if executor_id:
            executor = self.get_checked_user()
            if executor is None:
                raise exceptions.NotFoundError(executor_id, "executor")

        return self.query.get_filtered_tasks(filter_, executor_id, title_pattern)

    @log_exception
    @log_function_call
    def get_checked_filtered_groups(self,
                                    creator_id=None,
                                    task_id=None,
                                    ):
        filter_ = {}
        if creator_id:
            creator = self.get_checked_user()
            if creator is None:
                raise exceptions.NotFoundError(creator_id, "creator")
            creator_id = creator.user_id

        filter_[GroupAttribute.CREATOR_ID.value] = creator_id

        return self.query.get_filtered_groups(filter_, task_id)

    @log_exception
    @log_function_call
    def get_checked_filtered_plans(self, user_id=None, task_id=None):

        filter_ = {}
        if task_id:
            filter_[PlanAttribute.TASK_ID.value] = task_id

        user = self.get_checked_user()
        if user is None:
            raise exceptions.NotFoundError(user, "user")
        user_id = user.user_id

        return self.query.get_filtered_plans(filter_, user_id=user_id)

    # endregion
    @log_exception
    @log_function_call
    def update_application(self):
        """ Update application: alerts, overdue plans and task. """
        self._update_alerts()
        self._update_finished_plans()
        self._update_all_overdue_tasks()
