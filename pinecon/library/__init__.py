"""
    This package offers all the necessary classes and functions for working with pinecon library.

    Packages:
    ----------------
        models - provides modules, to represent abstractions for the pinecon library;
        services - provides modules to work with library at a higher level (with exception handling);
        storage - provides modules to work with library at a low level (databases and queries to it);

    How to use it:
    ----------------
        >>> import pinecon.library
        >>> from pinecon.library.services import service
        >>> from pinecon.library.storage import queries
        >>> from pinecon.library.storage import storage
        >>> connection_string = 'sqlite:///library.db'
        >>> data_storage = storage.Storage(connection_string)
        >>> queries = queries.Queries(data_storage)
        >>> serv = service.Service(queries)
        >>> from pinecon.library.models import model
        >>> user = serv.add_checked_user(model.User("Ligonberry"))
        >>> task = serv.add_checked_task(model.Task(user.user_id, "Learning Python"))

"""