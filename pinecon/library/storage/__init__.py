"""
    This package provides modules to work with library at a low level (databases and queries to it).

    Modules:
    ----------------
        queries - contains a class and a set of functions for querying the database;
        storage - provides class for keeping data in database using SQLAlchemy ORM;

    How to use it:
    ----------------
        >>> import pinecon.library
        >>> from pinecon.library.services import service
        >>> from pinecon.library.storage import queries
        >>> from pinecon.library.storage import storage
        >>> connection_string = 'sqlite:///library.db'
        >>> data_storage = storage.Storage(connection_string)
        >>> queries = queries.Queries(data_storage)
        >>> serv = service.Service(queries)
        >>> from pinecon.library.models import model
        >>> user = serv.add_checked_user(model.User("Ligonberry"))
        >>> task = serv.add_checked_task(model.Task(user.user_id, "Learning Python"))
"""
