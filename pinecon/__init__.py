import os
from pathlib import Path

from pinecon.library.services.service import Service
from pinecon.library.storage.queries import Queries
from pinecon.library.storage.storage import Storage
from pinecon.library.services import loggers

from secret_settings import DB_NAME, DB_LOGIN, DB_PASSWORD


storage = Storage("postgresql://{login}:{password}@localhost:5432/{name}".format(login=DB_LOGIN,
                                                                                 password=DB_PASSWORD,
                                                                                 name=DB_NAME))
query = Queries(storage)
service = Service(query)

loggers.set_logger(log_path=os.path.join(str(Path.home()), 'pineserver.log'))


def get_service():
    return service

