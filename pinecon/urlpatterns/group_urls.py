from django.conf.urls import url

from pinecon import views

urlpatterns = [
    url(r"^create/$", views.create_group, name="create_group"),
    url(r"^show/(?P<group_id>[0-9]+)$", views.show_group, name="show_group"),
    url(r"^update/(?P<group_id>[0-9]+)$", views.update_group, name="update_group"),
    url(r"^delete/(?P<group_id>[0-9]+)$", views.delete_group, name="delete_group"),

    url(r"^modify_group_to_task_relation/$", views.modify_group_to_task_relation, name="modify_group_to_task_relation"),
    url(r"^get_group_tasks/$", views.get_group_tasks, name="get_group_tasks"),
]
