from django.conf.urls import url

from pinecon import views

urlpatterns = [
    url(r"^create/(?P<task_id>[0-9]+)$", views.create_date, name="create_date"),
    url(r"^update/(?P<date_id>[0-9]+)$", views.update_date, name="update_date"),
    url(r"^delete/(?P<date_id>[0-9]+)$", views.delete_date, name="delete_date"),
]
