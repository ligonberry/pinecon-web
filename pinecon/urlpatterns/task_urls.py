from django.conf.urls import url

from pinecon import views

urlpatterns = [
    url(r"^create/$", views.create_task, name="create_task"),
    url(r"^show/(?P<task_id>[0-9]+)$", views.show_task, name="show_task"),
    url(r"^update/(?P<task_id>[0-9]+)$", views.update_task, name="update_task"),
    url(r"^finish/(?P<task_id>[0-9]+)$", views.finish_task, name="finish_task"),
    url(r"^delete/(?P<task_id>[0-9]+)$", views.delete_task, name="delete_task"),
    url(r"^delete_executor_to_task_relation/(?P<task_id>[0-9]+)$",
        views.delete_executor_to_task_relation, name="delete_executor_to_task_relation"),

    url(r"^create_subtask/(?P<task_id>[0-9]+)$", views.create_subtask, name="create_subtask"),
    url(r"^create_executor/(?P<task_id>[0-9]+)$", views.create_executor, name="create_executor"),

    url(r"^get_subtasks/$", views.get_subtasks, name="get_subtasks"),
    url(r"^get_tasks/$", views.get_tasks, name="get_tasks"),
    url(r"^add_task_to_task_relation/$", views.add_task_to_task_relation, name="add_task_to_task_relation"),
    url(r"^add_executor_to_task_relation/$", views.add_executor_to_task_relation, name="add_executor_to_task_relation"),
]
