from django.conf.urls import url

from pinecon import views

urlpatterns = [
    url(r"^create/$", views.create_plan, name="create_plan"),
    url(r"^update/(?P<plan_id>[0-9]+)$", views.update_plan, name="update_plan"),
]
