from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.decorators import login_required
from django.forms import HiddenInput
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect

from pinecon import get_service
from pinecon.forms import TaskForm, DateForm, GroupForm, PlanForm, UserCreateForm
from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelation
from pinecon.library.models.model.group import Group
from pinecon.library.models.model.group_to_task import GroupToTaskRelation
from pinecon.library.models.model.plan import PlanEndReason, Plan
from pinecon.library.models.model.task import TaskStatus, Task, TaskType
from pinecon.library.models.model.task_date import TaskDate, TaskDateType
from pinecon.library.models.model.task_to_task import TaskToTaskRelation
from pinecon.library.models.model.user import User
from pinecon.library.services.exceptions import PineconException


def login(request):
    username = request.user.username
    user = get_service().get_checked_user_by_username(username)
    get_service().current_user_id = user.user_id
    return user


def register(request):
    if request.method == "POST":
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            data = form.cleaned_data
            username = data["username"]
            password = data["password1"]
            user = authenticate(username=username, password=password)
            auth_login(request, user)
            get_service().add_checked_user(User(user.username, user.id))
            return HttpResponseRedirect(reverse("pinecon:home"))
    else:
        form = UserCreateForm()

    context = {"form": form}
    return render(request=request, template_name="pinecon/register.html", context=context)


@login_required(login_url="/login")
def home(request):
    user = login(request)

    all_tasks = get_service().get_checked_filtered_tasks(creator_id=user.user_id, type=TaskType.SIMPLE.value)
    plans = get_service().get_checked_plan(user_id=user.user_id)
    template_tasks = get_service().get_checked_filtered_tasks(creator_id=user.user_id, type=TaskType.PLAN.value)

    plans_and_templates = list(zip(plans, template_tasks))

    groups = get_service().get_checked_filtered_groups(creator_id=user.user_id)
    executor_tasks = get_service().get_checked_filtered_tasks(executor_id=user.user_id)
    overdue_tasks = get_service().get_checked_filtered_tasks(creator_id=user.user_id,
                                                             status=TaskStatus.OVERDUE.value)
    finished_tasks = get_service().get_checked_filtered_tasks(creator_id=user.user_id,
                                                              status=TaskStatus.EXECUTED.value)

    tasks = list(set(all_tasks) - set(finished_tasks) - set(overdue_tasks))

    context = {
        "tasks": tasks,
        "plans_and_templates": plans_and_templates,
        "groups": groups,
        "executor_tasks": executor_tasks,
        "overdue_tasks": overdue_tasks,
        "finished_tasks": finished_tasks,
    }

    get_service().update_application()

    return render(request=request, template_name="pinecon/home.html", context=context)


@login_required(login_url="/login")
def create_task(request):
    user = login(request)

    if request.method == "POST":
        task_form = TaskForm(request.POST)
        if task_form.is_valid():
            data = task_form.cleaned_data

            task = get_service().add_checked_task(Task(creator_id=user.user_id,
                                                       title=data["title"],
                                                       priority=data["priority"],
                                                       description=data["description"]))

            end_date = data["end_date"]
            start_date = data["start_date"]
            if end_date and start_date:
                get_service().add_checked_date(TaskDate(task.task_id, TaskDateType.START.value, start_date))
                get_service().add_checked_date(TaskDate(task.task_id, TaskDateType.END.value, end_date))
            elif end_date:
                get_service().add_checked_date(TaskDate(task.task_id, TaskDateType.END.value, end_date))
            elif start_date:
                get_service().add_checked_date(TaskDate(task.task_id, TaskDateType.EVENT.value, start_date))

            return HttpResponseRedirect(reverse("pinecon:home"))
    else:
        task_form = TaskForm()

    task_form.fields["status"].widget = HiddenInput()

    context = {"form": task_form}
    return render(request=request, template_name="pinecon/task/create.html", context=context)


@login_required(login_url="/login")
def show_task(request, task_id):
    user = login(request)

    task = get_service().get_checked_task_by_id(task_id)

    plan = None
    if task.task_type == TaskType.PLAN.value:
        plan = get_service().get_checked_filtered_plans(task_id=task_id)[0]

    task_dates = get_service().get_all_checked_task_dates(task_id=task_id)

    task_groups = get_service().get_checked_filtered_groups(creator_id=user.user_id,
                                                       task_id=task_id)

    task_executors = get_service().get_checked_executors(task_id=task_id)
    task_creator = get_service().is_creator(executor_id=user.user_id, task_id=task_id)
    print(task_creator)
    context = {
        "task": task,
        "plan": plan,
        "dates": task_dates,
        "groups": task_groups,
        "executors": task_executors,
        "creator": task_creator
    }

    return render(request=request, template_name="pinecon/task/show.html", context=context)


@login_required(login_url="/login")
def update_task(request, task_id):
    login(request)

    task = get_service().get_checked_task_by_id(task_id)

    if request.method == 'POST':
        task_form = TaskForm(request.POST)
        if task_form.is_valid():
            data = task_form.cleaned_data

            get_service().update_checked_task(task_id=task_id,
                                              title=data["title"],
                                              priority=data["priority"],
                                              description=data["description"],
                                              status=data["status"])

            return HttpResponseRedirect(reverse('pinecon:show_task', args=(task_id,)))
    else:
        task_form = TaskForm(initial={
            "title": task.title,
            "priority": task.priority,
            "description": task.description,
            "status": task.status
        })

        task_form.fields["start_date"].widget = HiddenInput()
        task_form.fields["end_date"].widget = HiddenInput()

        if task.task_type == TaskType.PLAN.value:
            task_form.fields["status"].widget = HiddenInput()
        elif task.status == TaskStatus.CREATED.value:
            choices = [(status.value, status.value) for status in TaskStatus]
            choices.remove((TaskStatus.DEFINED.value, TaskStatus.DEFINED.value))
            task_form.fields["status"].choices = choices
        else:
            choices = [(status.value, status.value) for status in TaskStatus]
            choices.remove((TaskStatus.DEFINED.value, TaskStatus.DEFINED.value))
            choices.remove((TaskStatus.CREATED.value, TaskStatus.CREATED.value))
            task_form.fields["status"].choices = choices

    context = {
        "form": task_form,
        "task_id": task_id
    }

    return render(request=request, template_name="pinecon/task/update.html", context=context)


@login_required(login_url="/login")
def finish_task(request, task_id):
    login(request)

    get_service().update_checked_task(task_id, status='executed')

    return HttpResponseRedirect(reverse("pinecon:home"))


@login_required(login_url="/login")
def delete_task(request, task_id):
    login(request)

    get_service().delete_checked_task_with_relations(task_id)

    return HttpResponseRedirect(reverse("pinecon:home"))


@login_required(login_url="/login")
def create_subtask(request, task_id):
    context = {
        "task_id": task_id
    }

    return render(request=request, template_name="pinecon/task/subtask/create.html", context=context)


@login_required(login_url="/login")
def create_executor(request, task_id):
    login(request)

    task = get_service().get_checked_task_by_id(task_id)

    context = {
        "task": task
    }

    return render(request=request, template_name="pinecon/task/executor/create.html", context=context)


@login_required(login_url="/login")
def create_date(request, task_id):
    login(request)

    if request.method == "POST":
        date_form = DateForm(request.POST)
        if date_form.is_valid():
            try:
                data = date_form.cleaned_data

                date = get_service().add_checked_date(TaskDate(task_id=task_id,
                                                               date=data["date"],
                                                               date_type=data["date_type"]))
                return HttpResponseRedirect(reverse('pinecon:show_task', args=(date.task_id,)))

            except PineconException as exception:
                date_form.add_error('date', str(exception))

    else:
        date_form = DateForm()

    context = {
        "task_id": task_id,
        "form": date_form
    }

    return render(request=request, template_name="pinecon/date/create.html", context=context)


@login_required(login_url="/login")
def update_date(request, date_id):
    login(request)

    date = get_service().get_checked_date_by_id(date_id)

    if request.method == "POST":
        date_form = DateForm(request.POST)
        if date_form.is_valid():
            data = date_form.cleaned_data

            date = get_service().update_checked_date(date_id=date_id,
                                                     date=data["date"])

            return HttpResponseRedirect(reverse('pinecon:show_task', args=(date.task_id,)))

    else:
        date_form = DateForm(initial={
            "date": date.date
        })

    date_form.fields["date_type"].widget = HiddenInput()

    context = {
        "date": date,
        "task_id": date.task_id,
        "form": date_form
    }

    return render(request=request, template_name="pinecon/date/update.html", context=context)


@login_required(login_url="/login")
def delete_date(request, date_id):
    login(request)

    task_id = get_service().get_checked_date_by_id(date_id).task_id
    get_service().delete_checked_date_by_id(date_id)

    return HttpResponseRedirect(reverse('pinecon:show_task', args=(task_id,)))


@login_required(login_url="/login")
def create_group(request):
    user = login(request)

    if request.method == "POST":
        group_form = GroupForm(user, request.POST)
        if group_form.is_valid():
            data = group_form.cleaned_data

            group = get_service().add_checked_group(Group(creator_id=user.user_id,
                                                          title=data["title"]))

            for task_id in data["tasks"]:
                get_service().add_checked_group_to_task_relation(GroupToTaskRelation(task_id=task_id,
                                                                                     group_id=group.group_id))

            return HttpResponseRedirect(reverse("pinecon:show_group", args=(group.group_id,)))

    else:
        group_form = GroupForm(user)

    context = {
        "form": group_form
    }

    return render(request=request, template_name="pinecon/group/create.html", context=context)


@login_required(login_url="/login")
def show_group(request, group_id):
    login(request)

    group = get_service().get_checked_group_by_id(group_id)
    group_tasks = get_service().get_all_checked_group_tasks(group_id)
    tasks_length = len(group_tasks)

    context = {
        "group": group,
        "tasks": group_tasks,
        "tasks_length": tasks_length
    }

    return render(request=request, template_name="pinecon/group/show.html", context=context)


@login_required(login_url="/login")
def update_group(request, group_id):
    login(request)

    if request.method == "POST":
        group = get_service().update_checked_group(group_id=group_id,
                                                   title=request.POST.get("title"))

        return JsonResponse({"success": True})

    group = get_service().get_checked_group_by_id(group_id)

    context = {
        "group": group
    }

    return render(request=request, template_name="pinecon/group/update.html", context=context)


@login_required(login_url="/login")
def delete_group(request, group_id):
    login(request)

    get_service().delete_checked_group_with_relations(group_id)

    return HttpResponseRedirect(reverse("pinecon:home"))


@login_required(login_url="/login")
def create_plan(request):
    user = login(request)

    if request.method == "POST":
        plan_form = PlanForm(request.POST)
        task_form = TaskForm(request.POST)
        if plan_form.is_valid() and task_form.is_valid():
            plan_data = plan_form.cleaned_data
            task_data = task_form.cleaned_data

            template = Task(creator_id=user.user_id,
                            title=task_data["title"],
                            status=TaskStatus.DEFINED.value,
                            task_type=TaskType.PLAN.value,
                            priority=task_data["priority"],
                            description=task_data["description"])

            repetitions_count = end_date = None
            if plan_data["end_reason"] == PlanEndReason.AFTER_NUMBER_OF_TIMES.value:
                repetitions_count = plan_data["repetitions_count"]
            if plan_data["end_reason"] == PlanEndReason.AFTER_DATE.value:
                end_date = plan_data["end_date"]

            plan = Plan(start_date=plan_data["start_date"],
                        repeat_style=plan_data["repetition_style"],
                        interval_counter=plan_data["interval_counter"],
                        end_reason=plan_data["end_reason"],
                        count=repetitions_count,
                        end_date=end_date)

            get_service().add_checked_plan(template=template, plan=plan)

            return HttpResponseRedirect(reverse("pinecon:home"))
    else:
        plan_form = PlanForm()
        task_form = TaskForm()

    plan_form.fields["is_active"].widget = HiddenInput()
    task_form.fields["status"].widget = HiddenInput()
    task_form.fields["start_date"].widget = HiddenInput()
    task_form.fields["end_date"].widget = HiddenInput()

    context = {
        "plan_form": plan_form,
        "task_form": task_form
    }

    return render(request=request, template_name="pinecon/plan/create.html", context=context)


@login_required(login_url="/login")
def update_plan(request, plan_id):
    login(request)

    plan = get_service().get_checked_plan_by_id(plan_id)

    if request.method == "POST":
        plan_form = PlanForm(request.POST)
        if plan_form.is_valid():
            data = plan_form.cleaned_data

            repetitions_count = end_date = None
            if data["end_reason"] == PlanEndReason.AFTER_NUMBER_OF_TIMES.value:
                repetitions_count = data["repetitions_count"]
            if data["end_reason"] == PlanEndReason.AFTER_DATE.value:
                end_date = data["end_date"]

            get_service().update_checked_plan(plan_id=plan_id,
                                              start_date=data["start_date"],
                                              repeat_style=data["repetition_style"],
                                              interval_counter=data["interval_counter"],
                                              end_reason=data["end_reason"],
                                              count=repetitions_count,
                                              end_date=end_date,
                                              active=data["is_active"])

            return HttpResponseRedirect(reverse('pinecon:show_task', args=(plan.task_id,)))
    else:
        plan_form = PlanForm(initial={
            "start_date": plan.start_date,
            "repetition_style": plan.repeat_style,
            "interval_counter": plan.interval_counter,
            "end_reason": plan.end_reason,
            "end_date": plan.end_date,
            "repetitions_count": plan.count,
            "is_active": plan.active
        })

    context = {
        "plan_id": plan_id,
        "task_id": plan.task_id,
        "form": plan_form
    }

    return render(request=request, template_name="pinecon/plan/update.html", context=context)


@csrf_protect
def add_task_to_task_relation(request):
    login(request)

    relation = TaskToTaskRelation(parent_task_id=int(request.POST.get("task_id")),
                                  subtask_id=int(request.POST.get("subtask_id")))

    try:
        get_service().add_checked_task_to_task_relation(relation)
    except PineconException as e:
        return JsonResponse({"success": False,
                             "error": str(e)})

    return JsonResponse({"success": True})


def get_tasks(request):
    user = login(request)

    simple_tasks = get_service().get_checked_filtered_tasks(creator_id=user.user_id,
                                                            type=TaskType.SIMPLE.value)

    tasks = [{"task_id": task.task_id,
              "title": task.title}
             for task in simple_tasks]

    return JsonResponse(tasks, safe=False)


def get_subtasks(request):
    login(request)

    subtasks = get_service().get_checked_subtasks(int(request.GET.get("task_id")))

    child_tasks = [{"child_id": task.task_id,
                    "child_title": task.title,
                    "number_of_child_tasks": len(subtasks)}
                   for task in subtasks]

    return JsonResponse(child_tasks, safe=False)


def get_executors(request):
    current_user = login(request)

    all_users = get_service().get_all_checked_users()
    current_users = [current_user]
    all_executors = list(set(all_users) - set(current_users))

    executors = [{"user_id": executor.user_id,
                  "username": executor.name}
                 for executor in all_executors]

    return JsonResponse(executors, safe=False)


@csrf_protect
def add_executor_to_task_relation(request):
    login(request)

    relation = ExecutorToTaskRelation(executor_id=int(request.POST.get("user_id")),
                                      task_id=int(request.POST.get("task_id")))

    try:
        get_service().add_checked_executor_to_task_relation(relation)
    except PineconException as e:
        return JsonResponse({"success": False,
                             "error": str(e)})

    return JsonResponse({"success": True})


def delete_executor_to_task_relation(request, task_id):
    user = login(request)

    try:
        get_service().delete_checked_executor_to_task_relation(task_id=task_id, executor_id=user.user_id)
    except PineconException as e:
        return JsonResponse({"success": False,
                             "error": str(e)})

    return HttpResponseRedirect(reverse("pinecon:home"))


def get_group_tasks(request):
    user = login(request)

    user_tasks = get_service().get_checked_filtered_tasks(creator_id=user.user_id)
    executor_tasks = get_service().get_checked_filtered_tasks(executor_id=user.user_id)

    all_tasks = user_tasks + list(set(executor_tasks) - set(user_tasks))
    group_tasks = get_service().get_all_checked_group_tasks(int(request.GET.get("group_id")))

    tasks_in_group = {task.task_id: task.title for task in group_tasks}
    tasks_ = {task.task_id: task.title for task in all_tasks}

    tasks = {"group_tasks": tasks_in_group,
             "all_tasks": tasks_}

    return JsonResponse(tasks)


@csrf_protect
def modify_group_to_task_relation(request):
    login(request)

    task_id = int(request.POST.get("task_id"))
    group_id = int(request.POST.get("group_id"))

    if request.POST.get("relation_add") == "true":
        get_service().add_checked_group_to_task_relation(GroupToTaskRelation(task_id=task_id,
                                                                             group_id=group_id))
    else:
        get_service().delete_checked_group_to_task_relation(task_id, group_id)

    return JsonResponse({"success": True})
