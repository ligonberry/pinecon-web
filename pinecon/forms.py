from datetime import datetime

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import ValidationError
from django.contrib.auth.forms import UserCreationForm

from pinecon import get_service
from pinecon.library.models import helpers
from pinecon.library.models.model.plan import PlanRepetitionStyle, PlanEndReason
from pinecon.library.models.model.task import TaskPriority, TaskStatus
from pinecon.library.models.model.task_date import TaskDateType

DEFAULT_EMPTY_LABEL = ("Choose Year", "Choose Month", "Choose Day")


class UserCreateForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)

        for fieldname in ['username', 'password1', 'password2']:
            self.fields[fieldname].help_text = None


class TaskForm(forms.Form):
    title = forms.CharField(label="Title", widget=forms.TextInput(attrs={'class': 'input'}))
    priority = forms.ChoiceField(choices=[(priority.value, priority.value) for priority in TaskPriority],
                                 initial=TaskPriority.MID.value)
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'textarea'}), required=False)
    status = forms.ChoiceField(choices=[(status.value, status.value) for status in TaskStatus],
                               required=False)

    start_date = forms.DateField(widget=forms.SelectDateWidget(empty_label=DEFAULT_EMPTY_LABEL), required=False)
    end_date = forms.DateField(widget=forms.SelectDateWidget(empty_label=DEFAULT_EMPTY_LABEL), required=False)

    def clean_date(self):
        form_data = self.cleaned_data
        start_date = form_data.get("start_date", None)
        end_date = form_data.get("end_date", None)
        if start_date is not None and end_date is not None:
            if end_date < start_date:
                raise ValidationError("End date sooner than start date")
        return start_date, end_date

    def clean_start_date(self):
        start_date, _ = self.clean_date()
        return start_date

    def clean_end_date(self):
        _, end_date = self.clean_date()
        return end_date


class DateForm(forms.Form):
    date = forms.DateField(widget=forms.SelectDateWidget,
                           initial=datetime.now().date())
    date_type = forms.ChoiceField(choices=[(date_type.value, date_type.value) for date_type in TaskDateType],
                                  initial=TaskDateType.EVENT.value)


class GroupForm(forms.Form):
    def __init__(self, user, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)

        user_tasks = get_service().get_checked_filtered_tasks(creator_id=user.user_id)
        executor_tasks = get_service().get_checked_filtered_tasks(executor_id=user.user_id)
        tasks = user_tasks + list(set(executor_tasks) - set(user_tasks))

        self.fields["tasks"].choices = [(task.task_id, task.title) for task in tasks]

    title = forms.CharField(label="Title", widget=forms.TextInput(attrs={'class': 'input'}))
    tasks = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(attrs={'class': 'select_multiple'}), required=False)


class PlanForm(forms.Form):
    repetition_style = forms.ChoiceField(choices=[(repetition_style.value, repetition_style.value)
                                                  for repetition_style in PlanRepetitionStyle],
                                         initial=PlanRepetitionStyle.DAILY.value)

    end_reason = forms.ChoiceField(choices=[(end_reason.value, end_reason.value) for end_reason in PlanEndReason],
                                   label="End Reason",
                                   initial=PlanEndReason.NEVER.value)

    interval_counter = forms.IntegerField(widget=forms.NumberInput, min_value=1, initial=1,
                                          label="Number of basic intervals")

    start_date = forms.DateField(
        widget=forms.SelectDateWidget,
        label="Task will begin on this day",
        initial=datetime.now().date())

    end_date = forms.DateField(widget=forms.SelectDateWidget(empty_label=DEFAULT_EMPTY_LABEL), required=False,
                               label="Task will be completed by this date")

    repetitions_count = forms.IntegerField(widget=forms.NumberInput, min_value=1, required=False,
                                           label="Number of times this task will be created")

    is_active = forms.ChoiceField(choices=[(helpers.NO, helpers.NO),
                                           (helpers.YES, helpers.YES)],
                                  required=False)

    def clean_repetitions_count(self):
        form_data = self.cleaned_data
        end_reason = form_data["end_reason"]
        repetitions_count = form_data.get("repetitions_count", None)

        if end_reason == PlanEndReason.AFTER_NUMBER_OF_TIMES.value and repetitions_count is None:
            raise ValidationError("Enter repetitions count")

        return repetitions_count

    def clean_start_date(self):
        form_data = self.cleaned_data
        start_date = form_data["start_date"]

        if start_date < datetime.now().date():
            raise ValidationError("Start date cannot be in the past")

        return start_date

    def clean_end_date(self):
        form_data = self.cleaned_data
        end_reason = form_data["end_reason"]
        end_date = form_data.get("end_date", None)

        if end_reason == PlanEndReason.AFTER_DATE.value and end_date is None:
            raise ValidationError("Choose end date")

        if end_date:
            start_date = form_data["start_date"]
            if end_date < start_date:
                raise ValidationError("End date sooner than start date")

        return end_date
