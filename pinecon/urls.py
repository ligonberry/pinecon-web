from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^register$', views.register, name='register'),
    url(r'^home', views.home, name='home'),
    url(r'^login', auth_views.login, {"template_name": "pinecon/login.html"}, name='login'),
    url(r'^logout/$', auth_views.logout, {"next_page": "/login"}, name='logout'),

    url(r'^task/', include('pinecon.urlpatterns.task_urls')),
    url(r'^plan/', include('pinecon.urlpatterns.plan_urls')),
    url(r'^group/', include('pinecon.urlpatterns.group_urls')),
    url(r'^date/', include('pinecon.urlpatterns.date_urls')),

    url(r"^get_executors/$", views.get_executors, name="get_executors"),
]
