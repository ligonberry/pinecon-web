# Pinecon Web Application
> Pinecon allows you to manage your tasks in the your favourite browser. It is simple, very fast, and stays out of the way. 

Memory like a sieve? Now no need to remember all 
those things that you have to do, because Pinecon will do that for you! 
The application will help you to remind both about important events (birthdays, holidays) 
and about daily routine. You can create simple tasks and plans, group them 
and give them to other users for execution.


![](screenshots/header.jpg)

## Meta

Darya Litvinchuk ```ligonberry@yandex.by```

Distributed under the MIT license. See ``LICENSE`` for more information.


## Contributing

1. Fork it (<https://bitbucket.org/ligonberry/fourth-python-lab/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
